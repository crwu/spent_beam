#!/usr/bin/env python3

import numpy as np
import unittest
from spentbeam.io import read_esray, write_esray
import spentbeam.measure as m
import tempfile


class Test(unittest.TestCase):

    def test_esray_write(self):
        filename = "examples/W7X_950kW.inj"
        beam1 = read_esray(filename, 0)
        with tempfile.NamedTemporaryFile(buffering=0, delete=True) as f:
            write_esray(f.name, beam1)
            beam2 = read_esray(f.name, 0)
        # do not use np.allclose() since it does not give diagnostics
        for i in range(beam1.shape[0]):
            for j in range(beam1.shape[1]):
                self.assertAlmostEqual(
                    beam1[i, j], beam2[i, j],
                    delta=abs(beam2[i,j]*1e-6),
                    msg="(i,j)=({},{})".format(i, j)
                )


    def test_esray_reader(self):
        filename = "examples/W7X_950kW.inj"
        beam = read_esray(filename, 0)
        es = np.loadtxt(filename)
        energy1 = (beam[:, 3]**2 + beam[:, 4]**2)
        energy2 = (es[:, 4]**2 + es[:, 5]**2)
        self.assertLess(max(abs(energy1 - energy2)), min(abs(energy1))*1e-12)

    def test_e_kin(self):
        filename = "examples/W7X_950kW.inj"
        beam = read_esray(filename, 0)
        e_kin = m.e_kin(beam)
        self.assertLess(30, min(e_kin))
        self.assertGreater(150, max(e_kin))

    def test_r_gc(self):
        filename = "examples/Iter_1.1MW_long.inj"
        beam = read_esray(filename, 0)
        rb = m.xy2rphi(beam[:, 0:2])[:, 0]
        rgc = m.rphi_gc(beam, 6.19)[:, 0]
        self.assertLess(max(rgc), max(rb))
        self.assertGreater(min(rgc), min(rb))

    def test_pitch_angle(self):
        filename = "examples/Iter_1.1MW_long.inj"
        beam = read_esray(filename, 0)
        a = m.pitch_angle(beam)
        self.assertLess(0.2, min(a))
        self.assertGreater(1.6, max(a))


if __name__ == "__main__":
    unittest.main()
