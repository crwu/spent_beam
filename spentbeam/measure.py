#!/usr/bin/env python3

import numpy as np
from spentbeam import C02


def v_total_2(beam):
    return beam[:, 3]**2 + beam[:, 4]**2 + beam[:, 5]**2


def gamma(beam):
    return 1.0 / np.sqrt(1 - v_total_2(beam)/C02)


def e_kin(beam):
    """Returns energy in keV"""
    return 511.0*(gamma(beam) - 1)


def pos_gc(beam, bz):
    """Return array of guiding center x, y"""
    gc = np.copy(beam[:, 0:2])
    factor = beam[:, 7]/beam[:, 6] / bz
    gc[:, 0] += factor*beam[:, 4]
    gc[:, 1] -= factor*beam[:, 3]
    return gc


def rphi_gc(beam, bz):
    """Return array of guiding center x, y"""
    return xy2rphi(pos_gc(beam, bz))


def xy2rphi(xy):
    assert xy.shape[1] == 2
    rphi = np.zeros_like(xy)
    c = xy[:, 0] + 1j*xy[:, 1]
    rphi[:, 0] = np.abs(c)
    rphi[:, 1] = np.angle(c)
    return rphi


def _angle(a):
    return np.angle(a[:, 0]+1j*a[:, 1])


def phase(beam, xy_gc=None, bz=None):
    """Return the phase of electrons according to guiding center"""
    if (xy_gc is not None and bz) or not (xy_gc is not None or bz):
        raise ValueError("Only one of xy_gc and bz has to be given.")
    if bz:
        xy_gc = pos_gc(beam, bz)
    cb = beam[:, 0:2] - xy_gc[:, 0:2]
    acb = _angle(cb)
    agc = _angle(xy_gc)
    return (acb-agc+np.pi) % (2*np.pi)-np.pi


def pitch_angle(beam):
    return np.sqrt(beam[:, 3]**2 + beam[:, 4]**2)/beam[:, 5]
