#!/usr/bin/env python3

"""
Internal format of the spent electron beam is an array with 9 columns:
    0, 1, 2,  3,  4,  5, 6,  7, 8
    x, y, z, vx, vy, vz, q, m0, i
"""

M0 = 9.10938356e-31
Q = -1.60217662e-19
C0 = 299792458.0
C02 = C0*C0
