#!/usr/bin/env python3

import numpy as np
from math import sin, cos


def rotate(beam, rad):
    res = np.zeros_like(beam)
    res[:, 2] = beam[:, 2]
    res[:, 5:] = beam[:, 5:]
    s = sin(rad)
    c = cos(rad)
    res[:, 0] = c*beam[:, 0] - s*beam[:, 1]
    res[:, 1] = s*beam[:, 0] + c*beam[:, 1]
    res[:, 3] = c*beam[:, 3] - s*beam[:, 4]
    res[:, 4] = s*beam[:, 3] + c*beam[:, 4]
    return res
