#!/usr/bin/env python3

import numpy as np
from spentbeam import M0, Q, C0


def write_cst_pid(filename, beam):
    pid = np.zeros([beam.shape[0], 9])
    pid[:, :4] = beam[:, :4]
    pid[:, [6, 7, 8]] = beam[:, [7, 6, 8]]
    pid[:, 3:6] = beam[:, 3:6]/C0  # becomes beta
    gamma = 1/np.sqrt(1-(pid[:, 3]**2+pid[:, 4]**2 + pid[:, 5]**2))
    for i in range(3, 6):
        pid[:, i] *= gamma
    np.savetxt(filename, pid)


def write_esray(filename, beam):
    es = np.zeros([beam.shape[0], 10])
    es[:, 0] = beam[:, 2]
    es[:, 3] = beam[:, 5]
    es[:, 6] = beam[:, 6]
    r = np.linalg.norm(beam[:, 0:2], axis=1)
    es[:, 1] = r
    es[:, 2] = np.angle(beam[:, 0] + 1j*beam[:, 1])
    nr = beam[:, 0:2].copy()
    nr[:, 0] /= r
    nr[:, 1] /= r
    # dot-prod projects v to r
    es[:, 4] = beam[:, 3]*nr[:, 0] + beam[:, 4]*nr[:, 1]
    # dot-prod projects v to r
    es[:, 5] = beam[:, 4]*nr[:, 0] - beam[:, 3]*nr[:, 1]
    np.savetxt(filename, es)


def read_esray(filename, current):
    #
    # convert esray input:
    #
    # 0:z      1:r          2:phi
    # 3:v_z    4:v_r        5:v_phi
    # 6:q      7:xstart     8:ipos  9:jpos
    #
    es = np.loadtxt(filename)
    num = es.shape[0]
    res = np.zeros([num, 9])
    cs = np.zeros([num, 2])
    nr = cs
    cs[:, 0] = np.cos(es[:, 2])
    cs[:, 1] = np.sin(es[:, 2])
    res[:, 0] = cs[:, 0] * es[:, 1]                          # x
    res[:, 1] = cs[:, 1] * es[:, 1]                          # y
    res[:, 2] = es[:, 0]                                     # z
    # dot products to project velocities:
    res[:, 3] = nr[:, 0] * es[:, 4] - nr[:, 1] * es[:, 5]  # vx
    res[:, 4] = nr[:, 1] * es[:, 4] + nr[:, 0] * es[:, 5]  # vy
    res[:, 5] = es[:, 3]                                     # vz
    res[:, 6] = es[:, 6] if es.shape[1] > 6 else np.ones(es.shape[0])*Q
    res[:, 7] = M0
    res[:, 8] = current / num
    return res


def read_ariadne(filename, current):
    """Return averaged B field"""
    dat = np.loadtxt(filename)
    num = dat.shape[0]
    res = np.zeros([num, 9])
    res[:, 0] = dat[:, 2]   # x
    res[:, 1] = dat[:, 3]   # y
    res[:, 2] = dat[:, 4]   # z
    factor = C0 / dat[:, 10]
    res[:, 3] = factor * dat[:, 7]   # vx
    res[:, 4] = factor * dat[:, 8]   # vy
    res[:, 5] = factor * dat[:, 9]  # vz
    res[:, 6] = np.ones(dat.shape[0])*Q
    res[:, 7] = M0
    res[:, 8] = current / num
    return res
