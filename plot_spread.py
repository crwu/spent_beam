#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from spentbeam.io import read_esray, read_ariadne
import spentbeam.measure as m


def handle_cmd_arg():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "inj",
        metavar="<esray beam injection file>",
        nargs=1, type=str
    )
    ap.add_argument(
        "-b", "--bz",
        metavar="<Bz at the beam interface>",
        nargs=1, type=float,
        dest="bz",
        required=True
    )
    ap.add_argument(
        "-n", "--nbins",
        metavar="<Number of bins to plot>",
        nargs=1, type=int,
        dest="nbins",
        default=[20],
        required=False
    )
    ap.add_argument(
        "-e", "--esray", "--euridice",
        action="store_const",
        const="esray",
        dest="filetype",
        default=None
    )
    ap.add_argument(
        "-a", "--ariadne",
        action="store_const",
        const="ariadne",
        dest="filetype"
    )
    return ap.parse_args()


def plot_phase(beam, bz, nbins=20):
    phase = m.phase(beam, bz=bz)
    rows = beam.shape[0]
    w = np.ones([rows, 1]) / rows * 100

    plt.figure()
    plt.grid()
    plt.xlabel("Phase / deg")
    plt.ylabel("Quota / %")
    plt.hist(phase, nbins, weights=w)


def plot_gc(beam, bz, nbins=20):
    rphi = m.rphi_gc(beam, bz)
    rows = beam.shape[0]
    w = np.ones([rows, 1]) / rows * 100

    plt.figure()
    plt.grid()
    plt.xlabel("Radius / mm")
    plt.ylabel("Quota / %")
    plt.hist(
        np.sqrt(beam[:, 0]**2 + beam[:, 1]**2)*1e3,
        nbins,
        weights=w,
        label="Beam electrons"
    )
    plt.hist(rphi[:, 0]*1e3, nbins, weights=w, label="Guiding centers")
    plt.legend(loc="best")

    plt.figure()
    plt.grid()
    plt.xlabel("Guiding center angle / deg")
    plt.ylabel("Quota / %")
    plt.hist(np.rad2deg(rphi[:, 1]), nbins, weights=w)


def plot_alpha(beam, nbins=20):
    alpha = m.pitch_angle(beam)
    rows = beam.shape[0]
    w = np.ones([rows, 1]) / rows * 100
    plt.figure()
    plt.grid()
    plt.xlabel("Pitch angle")
    plt.ylabel("Quota / %")
    plt.hist(alpha, nbins, weights=w)


def plot_e_kin(beam, nbins=40):
    ekin = m.e_kin(beam)
    rows = beam.shape[0]
    w = np.ones([rows, 1]) / rows * 100

    plt.figure()
    plt.grid()
    plt.xlabel("Kinetic energy / keV")
    plt.ylabel("Quota / %")
    plt.hist(ekin, nbins, weights=w)


def main():
    arg = handle_cmd_arg()
    inj_file = arg.inj[0]
    bz = arg.bz[0]
    nbins = arg.nbins[0]
    if arg.filetype == "esray":
        beam = read_esray(inj_file, 0)
    elif arg.filetype == "ariadne":
        beam = read_ariadne(inj_file, 0)
    else:
        print("please specify the file type, see help")
        return 1
    plot_e_kin(beam, nbins=nbins)
    plot_alpha(beam, nbins=nbins)
    plot_gc(beam, bz, nbins=nbins)
    plot_phase(beam, bz, nbins=nbins)
    plt.show()


if __name__ == "__main__":
    main()
