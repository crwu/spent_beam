#!/usr/bin/env python3

import numpy as np
from spentbeam.io import read_esray, read_ariadne, write_esray, write_cst_pid
from spentbeam import transform


def handle_cmd_arg():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "inj",
        metavar="<esray beam injection file>",
        nargs=1, type=str
    )
    ap.add_argument(
        "-o", "--output",
        metavar="<output file>",
        nargs=1, type=str,
        dest="output_file",
        required=True
    )
    ap.add_argument(
        "-f", "--output-format",
        metavar="<cst or esray>",
        nargs=1, type=str,
        dest="output_format",
        default=["cst"]
    )
    ap.add_argument(
        "-i", "--current",
        metavar="<beam current>",
        nargs=1, type=float,
        dest="current",
        required=True
    )
    ap.add_argument(
        "-d", "--duplications",
        metavar="<Number of duplications around the azimuthal angle>",
        nargs=1, type=int,
        dest="dup",
        required=True
    )
    ap.add_argument(
        "-e", "--esray", "--euridice",
        action="store_const",
        const="esray",
        dest="filetype",
        default=None
    )
    ap.add_argument(
        "-a", "--ariadne",
        action="store_const",
        const="ariadne",
        dest="filetype"
    )
    return ap.parse_args()


def main():
    arg = handle_cmd_arg()
    if arg.current[0] < 0:
        raise ValueError("Beam current should be non-negative.")
    if arg.filetype == "esray":
        ibeam = read_esray(arg.inj[0], arg.current[0])
    elif arg.filetype == "ariadne":
        ibeam = read_ariadne(arg.inj[0], arg.current[0])
    else:
        raise ValueError("No input file type is given.")
    if arg.dup[0] <= 0:
        raise ValueError("There should be more than one duplication.")
    ibeam[:, 8] /= arg.dup[0] + 1
    obeam = ibeam
    angle = 2*np.pi/(arg.dup[0]+1)
    for i in range(arg.dup[0]):
        obeam = np.append(obeam, transform.rotate(ibeam, angle*(i+1)), axis=0)
    if arg.output_format[0] == "esray":
        write_esray(arg.output_file[0], obeam)
    elif arg.output_format[0] == "cst":
        write_cst_pid(arg.output_file[0], obeam)
    else:
        raise NotImplementedError(
            "Format '{}' is not supported for output".format(
                arg.output_format[0]
            )
        )


if __name__ == "__main__":
    main()
